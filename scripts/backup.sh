#!/bin/bash

# Copies dot files, configurations, and other stuff I want to back up to the directory this resides in (for git)
thisdir=$(dirname `which $0`)

cat "$thisdir/$1" | xargs -J % cp -RLv % $thisdir/../$1 
