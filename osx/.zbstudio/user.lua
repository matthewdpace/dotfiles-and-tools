--[[--
  Use this file to specify User preferences.
  Review [examples](+/Applications/ZeroBraneStudio.app/Contents/ZeroBraneStudio/cfg/user-sample.lua) or check [online documentation](http://studio.zerobrane.com/documentation.html) for details.
--]]--

local G = ...
styles = G.loadfile('cfg/tomorrow.lua')('TomorrowNightEighties') -- theme
--[[night mode
styles.text = {
  bg = {0, 0, 0},
  fg = {220,220,220},
  sel = {70,70,70},
  keywords0 = {80, 110, 180},
  
}
]]
--editor
editor.autoreload = true
editor.fontname = "Anonymous Pro"
editor.fontsize = 14

path.moai = "/Applications/moai-dev/release/osx/host-sdl/bin/moai"
stylesoutshell = styles