

# Personal aliases and path settings
alias lsal="ls -a -G -F"
alias ls='ls -GFh'

export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

PATH=/Users/matthewpace/Documents/Scripts:${PATH}
export PATH=$PATH:/Applications/Postgres.app/Contents/Versions/9.4/bin


alias blender=/Applications/Blender.app/Contents/MacOS/blender


#TI MSP430
PATH=/Users/matthewpace/ti/gcc/bin:${PATH}

# Python

PATH=/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}
PATH=/Library/Frameworks/Python.framework/Versions/3.4/bin:${PATH}

# Python 3.4 virtualenvwrapper
#alias python='python3'
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/dev
export VIRTUALENVWRAPPER_PYTHON=/Library/Frameworks/Python.framework/Versions/3.4/bin/python3
source /Library/Frameworks/Python.framework/Versions/3.4/bin/virtualenvwrapper.sh
#export PYENV_VIRTUALENVWRAPPER_PREFER_PYVENV="true"


# Lua, Moai, Love

alias love=/Applications/love.app/Contents/MacOS/love
alias moaitest=/Applications/moai-dev/release/osx/host-sdl/bin/moai

export MOAI_BIN=/Applications/moai-dev/release/osx/host-sdl/bin
export MOAI_CONFIG=/Applications/moai-dev/samples/config


# Android
export ANDROID_NDK=/Applications/android-ndk-r10b
export ANDROID_SDK_ROOT=/Applications/adt-bundle-mac-x86_64-20140702/sdk
PATH=/Applications/android-ndk-r10b:/Applications/adt-bundle-mac-x86_64-20140702/sdk/platform-tools:/Applications/adt-bundle-mac-x86_64-20140702/sdk/tools:${PATH}


# Databases

PATH=/Applications/Postgres.app/Contents/Versions/9.4/bin:$PATH
PATH=/usr/local/mysql-5.6.13-osx10.7-x86_64/bin:$PATH



export DYLD_FALLBACK_LIBRARY_PATH=/Applications/Postgres.app/Contents/MacOS/lib:$DYLD_LIBRARY_PATH



### Added by the Heroku Toolbelt
PATH=/usr/local/heroku/bin:$PATH



# gameplay3d
PATH=/Users/matthewpace/Documents/Github/Gameplay:$PATH


# cocos2d-x
# temporarily disabled sticking with moai
# export COCOS_CONSOLE_ROOT=/Users/matthewpace/cocos2d-x-3.3beta0/tools/cocos2d-console/bin
# export PATH=$COCOS_CONSOLE_ROOT:$PATH


# Add environment variable NDK_ROOT for cocos2d-x
#export NDK_ROOT=/Applications/android-ndk-r9d
#export PATH=$NDK_ROOT:$PATH

# Add environment variable ANT_ROOT for cocos2d-x
#export ANT_ROOT=/usr/local/Cellar/ant/1.9.4/libexec/bin
#export PATH=$ANT_ROOT:$PATH



# Add environment variable COCOS_CONSOLE_ROOT for cocos2d-x
export COCOS_CONSOLE_ROOT=/Applications/Cocos/frameworks/cocos2d-x-3.5/tools/cocos2d-console/bin
export PATH=$COCOS_CONSOLE_ROOT:$PATH

# Add environment variable COCOS_FRAMEWORKS for cocos2d-x
export COCOS_FRAMEWORKS=/Applications/Cocos/frameworks
export PATH=$COCOS_FRAMEWORKS:$PATH

# Add environment variable ANT_ROOT for cocos2d-x
export ANT_ROOT=/Applications/Cocos/tools/ant/bin
export PATH=$ANT_ROOT:$PATH

# Setting PATH for Python 3.4
# The orginal version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/3.4/bin:${PATH}"
export PATH
