colorscheme desert
set hidden
set smartindent
set expandtab
set nocompatible
syntax enable
colorscheme desert
set number
set background=dark
set whichwrap+=<,>,h,l,[,]
au FileType lua set tabstop=2 | set shiftwidth=2
au FileType python set tabstop=4 | set shiftwidth=4

execute pathogen#infect()
